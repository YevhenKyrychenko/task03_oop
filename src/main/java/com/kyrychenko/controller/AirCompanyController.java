package com.kyrychenko.controller;

import com.kyrychenko.model.AirCompany;
import com.kyrychenko.model.Plane;

import java.util.List;

public interface AirCompanyController {

    void createPlane(String name, int capacity, double carryingCapacity, double fuelConsumption, double flightRange);

    AirCompany createAirCompany(String name);

    int getTotalCapacity();

    double getTotalCarryingCapacity();

    List<Plane> getSortedPlanesByFlightRange();

    List<Plane> findPlanesByFuelConsumption(double from, double to);
}
