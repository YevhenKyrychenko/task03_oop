package com.kyrychenko.controller;

import com.kyrychenko.model.AirCompany;
import com.kyrychenko.model.AirCompanyBusinessLogic;
import com.kyrychenko.model.AirCompanyModel;
import com.kyrychenko.model.Plane;

import java.util.List;

public class AirCompanyControllerImpl implements AirCompanyController {
    private AirCompanyModel model;

    public AirCompanyControllerImpl() {
        this.model = new AirCompanyBusinessLogic();
    }

    @Override
    public void createPlane(String name, int capacity, double carryingCapacity,
                            double fuelConsumption, double flightRange) {
        this.model.addPlane(name, capacity, carryingCapacity, fuelConsumption, flightRange);
    }

    @Override
    public AirCompany createAirCompany(String name) {
        return this.model.createAirCompany(name);
    }

    @Override
    public int getTotalCapacity() {
        return this.model.countTotalCapacity();
    }

    @Override
    public double getTotalCarryingCapacity() {
        return this.model.countTotalCarryingCapacity();
    }

    @Override
    public List<Plane> getSortedPlanesByFlightRange() {
        return this.model.sortByFlightRange();
    }

    @Override
    public List<Plane> findPlanesByFuelConsumption(double from, double to) {
        return this.model.findPlanesByFuelConsumption(from, to);
    }
}
