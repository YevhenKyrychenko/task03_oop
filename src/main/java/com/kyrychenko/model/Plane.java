package com.kyrychenko.model;

public interface Plane {
    int getCapacity();

    double getCarryingCapacity();

    double getFuelConsumption();

    Double getFlightRange();
}
