package com.kyrychenko.model;

import java.util.List;

public interface AirCompanyModel {
    AirCompany createAirCompany(String name);

    void addPlane(String name, int capacity, double carryingCapacity, double fuelConsumption, double flightRange);

    int countTotalCapacity();

    double countTotalCarryingCapacity();

    List<Plane> sortByFlightRange();

    List<Plane> findPlanesByFuelConsumption(double from, double to);
}
