package com.kyrychenko.model;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AirCompanyBusinessLogic implements AirCompanyModel {
    private AirCompany airCompany;

    @Override
    public AirCompany createAirCompany(String name) {
        this.airCompany = new AirCompany(name);
        return this.airCompany;
    }

    @Override
    public void addPlane(String name, int capacity, double carryingCapacity,
                         double fuelConsumption, double flightRange) {
        this.airCompany.addPlane(new Airplane(name, capacity, carryingCapacity, fuelConsumption, flightRange));
    }

    @Override
    public int countTotalCapacity() {
        return airCompany.getPlanes().stream().mapToInt(plane -> plane.getCapacity()).sum();
    }

    @Override
    public double countTotalCarryingCapacity() {
        return airCompany.getPlanes().stream().mapToDouble(plane -> plane.getCarryingCapacity()).sum();
    }

    @Override
    public List<Plane> sortByFlightRange() {
        return airCompany.getPlanes().stream()
                .sorted(Comparator.comparing(Plane::getFlightRange))
                .collect(Collectors.toList());
    }

    @Override
    public List<Plane> findPlanesByFuelConsumption(double from, double to) {
        return airCompany.getPlanes().stream()
                .filter(plane -> plane.getFuelConsumption() >= from && plane.getFuelConsumption() <= to)
                .collect(Collectors.toList());
    }
}
