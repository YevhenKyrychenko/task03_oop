package com.kyrychenko.model;

public class Airplane implements Plane {
    private String name;
    private int capacity;
    private double carryingCapacity;
    private double fuelConsumption;
    private double flightRange;

    public Airplane(String name, int capacity, double carryingCapacity, double fuelConsumption, double flightRange) {
        this.name = name;
        this.capacity = capacity;
        this.carryingCapacity = carryingCapacity;
        this.fuelConsumption = fuelConsumption;
        this.flightRange = flightRange;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public double getCarryingCapacity() {
        return this.carryingCapacity;
    }

    public double getFuelConsumption() {
        return this.fuelConsumption;
    }

    @Override
    public Double getFlightRange() {
        return this.flightRange;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "name='" + name + '\'' +
                ", capacity=" + capacity +
                ", carryingCapacity=" + carryingCapacity +
                ", fuelConsumption=" + fuelConsumption +
                ", flightRange=" + flightRange +
                '}';
    }
}
