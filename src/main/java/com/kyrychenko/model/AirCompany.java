package com.kyrychenko.model;

import java.util.ArrayList;
import java.util.List;

public class AirCompany {
    private List<Plane> planes;
    private String name;

    public AirCompany(String name) {
        this.name = name;
        this.planes = new ArrayList<>();
    }

    public void addPlane(Plane plane) {
        this.planes.add(plane);
    }

    public List<Plane> getPlanes() {
        return this.planes;
    }
}
