package com.kyrychenko.view;

import com.kyrychenko.controller.AirCompanyController;
import com.kyrychenko.controller.AirCompanyControllerImpl;

public class AirportView {
    private AirCompanyController controller;

    public AirportView() {
        this.controller = new AirCompanyControllerImpl();
    }

    public void show() {
        this.controller.createAirCompany("AirFrance");
        this.controller.createPlane("Airbus380", 180, 12.0, 100.0, 2000);
        this.controller.createPlane("Boeing737", 240, 24.0, 160.0, 3700);
        this.controller.createPlane("Boeing777", 300, 30.0, 200.0, 2500);
        System.out.println("Total capacity:" + this.controller.getTotalCapacity());
        System.out.println("Total carrying capacity:" + this.controller.getTotalCarryingCapacity());
        System.out.println("Sorted by flight range list of planes :" + this.controller.getSortedPlanesByFlightRange());
        System.out.println("planes in diapason 10-160 l/km fuel consumption:"
                + this.controller.findPlanesByFuelConsumption(10, 160));
    }
}
